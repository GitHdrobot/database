@startuml

!theme plain
top to bottom direction
skinparam linetype ortho

class _trusted_assemblies {
   hash: varbinary(8000)
   description: nvarchar(4000)
   create_date: datetime2
   created_by: nvarchar(128)
}
class backup_metadata_store {
   backup_metadata_uuid: uniqueidentifier
   database_guid: uniqueidentifier
   physical_database_name: nvarchar(128)
   time_zone: smallint
   first_lsn: numeric(25)
   last_lsn: numeric(25)
   checkpoint_lsn: numeric(25)
   database_backup_lsn: numeric(25)
   backup_start_date: datetime2
   backup_finish_date: datetime2
   backup_type: char(1)
   backup_storage_redundancy: nvarchar(64)
   database_version: int
   backup_size: numeric(20)
   compressed_backup_size: numeric(20)
   server_name: nvarchar(128)
   is_damaged: bit
   last_recovery_fork_guid: uniqueidentifier
   differential_base_lsn: numeric(25)
   differential_base_guid: uniqueidentifier
   backup_path: nvarchar(260)
   last_valid_restore_time: datetime2
   compression_algorithm: nvarchar(32)
}
class db_ledger_blocks {
   block_id: bigint
   version: smallint
   transactions_root_hash: varbinary(32)
   block_size: int
   previous_block_hash: varbinary(32)
}
class db_ledger_digest_locations {
   storage_type: int
   path: nvarchar(4000)
   last_digest_block_id: bigint
   is_current: tinyint
}
class db_ledger_transactions {
   transaction_id: bigint
   block_id: bigint
   transaction_ordinal: int
   version: tinyint
   type: tinyint
   commit_ts: datetime2
   table_hashes: varbinary(max)
   commit_LSN: varbinary(10)
   transaction_description: nvarchar(max)
   principal_name: sysname
}
class external_libraries_installed_table {
   db_id: int
   principal_id: int
   language_id: int
   external_library_id: int
   name: nvarchar(260)
   mdversion: binary(8)
}
class external_library_setup_failures {
   db_id: int
   principal_id: int
   external_library_id: int
   error_code: int
   error_timestamp: datetime2
   error_message: nvarchar(1024)
}
class ledger_columns_history_internal {
   object_id: int
   column_id: int
   column_name: sysname
   operation_type: int
   ledger_start_transaction_id: bigint
   ledger_end_transaction_id: bigint
   ledger_start_sequence_number: bigint
   ledger_end_sequence_number: bigint
}
class ledger_columns_history_internal_history {
   object_id: int
   column_id: int
   column_name: sysname
   operation_type: int
   ledger_start_transaction_id: bigint
   ledger_end_transaction_id: bigint
   ledger_start_sequence_number: bigint
   ledger_end_sequence_number: bigint
}
class ledger_tables_history_internal {
   schema_name: sysname
   table_name: sysname
   object_id: int
   ledger_view_schema_name: sysname
   ledger_view_name: sysname
   operation_type: int
   ledger_start_transaction_id: bigint
   ledger_end_transaction_id: bigint
   ledger_start_sequence_number: bigint
   ledger_end_sequence_number: bigint
   status: int
   transaction_id_column_name: sysname
   sequence_number_column_name: sysname
   operation_type_column_name: sysname
   operation_type_desc_column_name: sysname
}
class ledger_tables_history_internal_history {
   schema_name: sysname
   table_name: sysname
   object_id: int
   ledger_view_schema_name: sysname
   ledger_view_name: sysname
   operation_type: int
   ledger_start_transaction_id: bigint
   ledger_end_transaction_id: bigint
   ledger_start_sequence_number: bigint
   ledger_end_sequence_number: bigint
   status: int
   transaction_id_column_name: sysname
   sequence_number_column_name: sysname
   operation_type_column_name: sysname
   operation_type_desc_column_name: sysname
}
class persistent_version_store {
   xdes_ts_push: bigint
   xdes_ts_tran: bigint
   subid_push: int
   subid_tran: int
   rowset_id: bigint
   sec_version_rid: binary(8)
   min_len: smallint
   seq_num: bigint
   prev_row_in_chain: binary(8)
   row_version: varbinary(8000)
}
class persistent_version_store_long_term {
   xdes_ts_push: bigint
   xdes_ts_tran: bigint
   subid_push: int
   subid_tran: int
   rowset_id: bigint
   sec_version_rid: binary(8)
   min_len: smallint
   seq_num: bigint
   prev_row_in_chain: binary(8)
   row_version: varbinary(8000)
}
class plan_persist_context_settings {
   context_settings_id: bigint
   set_options: int
   language_id: smallint
   date_format: smallint
   date_first: tinyint
   compatibility_level: smallint
   status: smallint
   required_cursor_options: int
   acceptable_cursor_options: int
   merge_action_type: smallint
   default_schema_id: int
   is_replication_specific: bit
   status2: tinyint
}
class plan_persist_plan {
   plan_id: bigint
   query_id: bigint
   plan_group_id: bigint
   engine_version: bigint
   query_plan_hash: binary(8)
   query_plan: varbinary(max)
   is_online_index_plan: bit
   is_trivial_plan: bit
   is_parallel_plan: bit
   is_forced_plan: bit
   force_failure_count: bigint
   last_force_failure_reason: int
   count_compiles: bigint
   initial_compile_start_time: datetimeoffset
   last_compile_start_time: datetimeoffset
   last_execution_time: datetimeoffset
   total_compile_duration: bigint
   last_compile_duration: bigint
   compatibility_level: smallint
   plan_flags: int
}
class plan_persist_plan_feedback {
   plan_feedback_id: bigint
   plan_id: bigint
   feature_id: tinyint
   feedback_data: varbinary(max)
   state: int
   create_time: datetimeoffset
   last_updated_time: datetimeoffset
   replica_group_id: bigint
}
class plan_persist_plan_forcing_locations {
   plan_forcing_location_id: bigint
   query_id: bigint
   plan_id: bigint
   replica_group_id: bigint
   timestamp: datetime
   plan_forcing_flags: int
}
class plan_persist_query {
   query_id: bigint
   query_text_id: bigint
   context_settings_id: bigint
   object_id: bigint
   batch_sql_handle: varbinary(64)
   query_hash: binary(8)
   is_internal_query: bit
   query_param_type: tinyint
   initial_compile_start_time: datetimeoffset
   last_compile_start_time: datetimeoffset
   last_execution_time: datetimeoffset
   last_compile_batch_sql_handle: varbinary(64)
   last_compile_batch_offset_start: bigint
   last_compile_batch_offset_end: bigint
   compile_count: bigint
   total_compile_duration: bigint
   last_compile_duration: bigint
   total_parse_duration: bigint
   last_parse_duration: bigint
   total_parse_cpu_time: bigint
   last_parse_cpu_time: bigint
   total_bind_duration: bigint
   last_bind_duration: bigint
   total_bind_cpu_time: bigint
   last_bind_cpu_time: bigint
   total_optimize_duration: bigint
   last_optimize_duration: bigint
   total_optimize_cpu_time: bigint
   last_optimize_cpu_time: bigint
   total_compile_memory_kb: bigint
   last_compile_memory_kb: bigint
   max_compile_memory_kb: bigint
   status: tinyint
   statement_sql_handle: varbinary(64)
   query_flags: int
}
class plan_persist_query_hints {
   query_hint_id: bigint
   query_id: bigint
   context_settings_id: bigint
   object_id: bigint
   statement_sql_handle: varbinary(64)
   query_param_type: tinyint
   batch_sql_handle: varbinary(64)
   query_hash: binary(8)
   query_hints: nvarchar(max)
   query_hints_flags: int
   last_query_hint_failure_reason: int
   query_hint_failure_count: bigint
   comment: nvarchar(max)
   replica_group_id: bigint
}
class plan_persist_query_template_parameterization {
   query_template_id: bigint
   query_template: nvarchar(max)
   query_template_hash: varbinary(16)
   query_param_type: tinyint
   query_template_flags: int
   status: tinyint
   last_parameterization_failure_reason: int
   parameterization_failure_count: bigint
   comment: nvarchar(max)
}
class plan_persist_query_text {
   query_text_id: bigint
   query_sql_text: nvarchar(max)
   statement_sql_handle: varbinary(64)
   is_part_of_encrypted_module: bit
   has_restricted_text: bit
   query_template_hash: varbinary(16)
}
class plan_persist_query_variant {
   query_variant_query_id: bigint
   parent_query_id: bigint
   dispatcher_plan_id: bigint
}
class plan_persist_replicas {
   replica_group_id: bigint
   role_type: smallint
   replica_name: nvarchar(644)
}
class plan_persist_runtime_stats {
   runtime_stats_id: bigint
   plan_id: bigint
   runtime_stats_interval_id: bigint
   execution_type: tinyint
   first_execution_time: datetimeoffset
   last_execution_time: datetimeoffset
   count_executions: bigint
   total_duration: bigint
   last_duration: bigint
   min_duration: bigint
   max_duration: bigint
   sumsquare_duration: float
   total_cpu_time: bigint
   last_cpu_time: bigint
   min_cpu_time: bigint
   max_cpu_time: bigint
   sumsquare_cpu_time: float
   total_logical_io_reads: bigint
   last_logical_io_reads: bigint
   min_logical_io_reads: bigint
   max_logical_io_reads: bigint
   sumsquare_logical_io_reads: float
   total_logical_io_writes: bigint
   last_logical_io_writes: bigint
   min_logical_io_writes: bigint
   max_logical_io_writes: bigint
   sumsquare_logical_io_writes: float
   total_physical_io_reads: bigint
   last_physical_io_reads: bigint
   min_physical_io_reads: bigint
   max_physical_io_reads: bigint
   sumsquare_physical_io_reads: float
   total_clr_time: bigint
   last_clr_time: bigint
   min_clr_time: bigint
   max_clr_time: bigint
   sumsquare_clr_time: float
   total_dop: bigint
   last_dop: bigint
   min_dop: bigint
   max_dop: bigint
   sumsquare_dop: float
   total_query_max_used_memory: bigint
   last_query_max_used_memory: bigint
   min_query_max_used_memory: bigint
   max_query_max_used_memory: bigint
   sumsquare_query_max_used_memory: float
   total_rowcount: bigint
   last_rowcount: bigint
   min_rowcount: bigint
   max_rowcount: bigint
   sumsquare_rowcount: float
   total_num_physical_io_reads: bigint
   last_num_physical_io_reads: bigint
   min_num_physical_io_reads: bigint
   max_num_physical_io_reads: bigint
   sumsquare_num_physical_io_reads: float
   total_log_bytes_used: bigint
   last_log_bytes_used: bigint
   min_log_bytes_used: bigint
   max_log_bytes_used: bigint
   sumsquare_log_bytes_used: float
   total_tempdb_space_used: bigint
   last_tempdb_space_used: bigint
   min_tempdb_space_used: bigint
   max_tempdb_space_used: bigint
   sumsquare_tempdb_space_used: float
   total_page_server_io_reads: bigint
   last_page_server_io_reads: bigint
   min_page_server_io_reads: bigint
   max_page_server_io_reads: bigint
   sumsquare_page_server_io_reads: float
}
class plan_persist_runtime_stats_interval {
   runtime_stats_interval_id: bigint
   start_time: datetimeoffset
   end_time: datetimeoffset
   comment: nvarchar(max)
}
class plan_persist_runtime_stats_v2 {
   runtime_stats_id: bigint
   plan_id: bigint
   runtime_stats_interval_id: bigint
   execution_type: tinyint
   first_execution_time: datetimeoffset
   last_execution_time: datetimeoffset
   count_executions: bigint
   total_duration: bigint
   last_duration: bigint
   min_duration: bigint
   max_duration: bigint
   sumsquare_duration: float
   total_cpu_time: bigint
   last_cpu_time: bigint
   min_cpu_time: bigint
   max_cpu_time: bigint
   sumsquare_cpu_time: float
   total_logical_io_reads: bigint
   last_logical_io_reads: bigint
   min_logical_io_reads: bigint
   max_logical_io_reads: bigint
   sumsquare_logical_io_reads: float
   total_logical_io_writes: bigint
   last_logical_io_writes: bigint
   min_logical_io_writes: bigint
   max_logical_io_writes: bigint
   sumsquare_logical_io_writes: float
   total_physical_io_reads: bigint
   last_physical_io_reads: bigint
   min_physical_io_reads: bigint
   max_physical_io_reads: bigint
   sumsquare_physical_io_reads: float
   total_clr_time: bigint
   last_clr_time: bigint
   min_clr_time: bigint
   max_clr_time: bigint
   sumsquare_clr_time: float
   total_dop: bigint
   last_dop: bigint
   min_dop: bigint
   max_dop: bigint
   sumsquare_dop: float
   total_query_max_used_memory: bigint
   last_query_max_used_memory: bigint
   min_query_max_used_memory: bigint
   max_query_max_used_memory: bigint
   sumsquare_query_max_used_memory: float
   total_rowcount: bigint
   last_rowcount: bigint
   min_rowcount: bigint
   max_rowcount: bigint
   sumsquare_rowcount: float
   total_num_physical_io_reads: bigint
   last_num_physical_io_reads: bigint
   min_num_physical_io_reads: bigint
   max_num_physical_io_reads: bigint
   sumsquare_num_physical_io_reads: float
   total_log_bytes_used: bigint
   last_log_bytes_used: bigint
   min_log_bytes_used: bigint
   max_log_bytes_used: bigint
   sumsquare_log_bytes_used: float
   total_tempdb_space_used: bigint
   last_tempdb_space_used: bigint
   min_tempdb_space_used: bigint
   max_tempdb_space_used: bigint
   sumsquare_tempdb_space_used: float
   total_page_server_io_reads: bigint
   last_page_server_io_reads: bigint
   min_page_server_io_reads: bigint
   max_page_server_io_reads: bigint
   sumsquare_page_server_io_reads: float
   replica_group_id: bigint
}
class plan_persist_wait_stats {
   wait_stats_id: bigint
   runtime_stats_interval_id: bigint
   plan_id: bigint
   wait_category: smallint
   execution_type: tinyint
   count_executions: bigint
   total_query_wait_time_ms: bigint
   last_query_wait_time_ms: bigint
   min_query_wait_time_ms: bigint
   max_query_wait_time_ms: bigint
   sumsquare_query_wait_time_ms: float
}
class plan_persist_wait_stats_v2 {
   wait_stats_id: bigint
   runtime_stats_interval_id: bigint
   plan_id: bigint
   wait_category: smallint
   execution_type: tinyint
   count_executions: bigint
   total_query_wait_time_ms: bigint
   last_query_wait_time_ms: bigint
   min_query_wait_time_ms: bigint
   max_query_wait_time_ms: bigint
   sumsquare_query_wait_time_ms: float
   replica_group_id: bigint
}
class polaris_executed_requests_history {
   id: bigint
   distributed_statement_id: nvarchar(128)
   status: smallint
   transaction_id: bigint
   query_hash: binary(8)
   login_name: nvarchar(644)
   start_time: datetime2
   end_time: datetime2
   command: smallint
   total_elapsed_time_ms: bigint
   data_processed_mb: bigint
   error: nvarchar(max)
   error_code: int
   rejected_rows_path: nvarchar(max)
   sql_handle: varbinary(64)
   query_info: int
   statement_offset_start: int
   statement_offset_end: int
}
class polaris_executed_requests_text {
   id: bigint
   sql_handle: varbinary(64)
   sql_text: nvarchar(max)
   last_access: datetime2
}
class polaris_file_statistics {
   hash_key: uniqueidentifier
   stats_blob: varbinary(max)
   source_props: nvarchar(max)
   additional_props: nvarchar(max)
   is_auto: bit
   access_date: datetime2
}
class queue_messages_1003150619 {
   status: tinyint
   priority: tinyint
   queuing_order: bigint
   conversation_group_id: uniqueidentifier
   conversation_handle: uniqueidentifier
   message_sequence_number: bigint
   message_id: uniqueidentifier
   message_type_id: int
   service_id: int
   service_contract_id: int
   validation: nchar(1)
   next_fragment: int
   fragment_size: int
   fragment_bitmap: bigint
   binary_message_body: varbinary(max)
   message_enqueue_time: datetime
}
class queue_messages_1035150733 {
   status: tinyint
   priority: tinyint
   queuing_order: bigint
   conversation_group_id: uniqueidentifier
   conversation_handle: uniqueidentifier
   message_sequence_number: bigint
   message_id: uniqueidentifier
   message_type_id: int
   service_id: int
   service_contract_id: int
   validation: nchar(1)
   next_fragment: int
   fragment_size: int
   fragment_bitmap: bigint
   binary_message_body: varbinary(max)
   message_enqueue_time: datetime
}
class queue_messages_1067150847 {
   status: tinyint
   priority: tinyint
   queuing_order: bigint
   conversation_group_id: uniqueidentifier
   conversation_handle: uniqueidentifier
   message_sequence_number: bigint
   message_id: uniqueidentifier
   message_type_id: int
   service_id: int
   service_contract_id: int
   validation: nchar(1)
   next_fragment: int
   fragment_size: int
   fragment_bitmap: bigint
   binary_message_body: varbinary(max)
   message_enqueue_time: datetime
}
class sql_pools_table {
   guid: uniqueidentifier
   name: nvarchar(128)
   state: nvarchar(128)
   service_objective: sysname
   max_service_objective: sysname
   service_tier: sysname
   auto_pause_timer: smallint
   is_auto_resume_on: bit
   is_result_set_caching_on: bit
   create_date: datetime
   modify_date: datetime
   owner_id: int
   connect_role_id: int
   alter_role_id: int
   id: int
}
class sqlagent_job_history {
   instance_id: int
   job_id: uniqueidentifier
   step_id: int
   sql_message_id: int
   sql_severity: int
   message: nvarchar(4000)
   run_status: int
   run_date: int
   run_time: int
   run_duration: int
   operator_id_emailed: int
   operator_id_paged: int
   retries_attempted: int
}
class sqlagent_jobs {
   job_id: uniqueidentifier
   name: sysname
   enabled: bit
   description: nvarchar(512)
   start_step_id: int
   notify_level_eventlog: bit
   delete_level: int
   date_created: datetime
   date_modified: datetime
}
class sqlagent_jobsteps {
   job_id: uniqueidentifier
   step_id: int
   step_name: sysname
   subsystem: nvarchar(40)
   command: nvarchar(max)
   flags: int
   additional_parameters: nvarchar(max)
   cmdexec_success_code: int
   on_success_action: tinyint
   on_success_step_id: int
   on_fail_action: tinyint
   on_fail_step_id: int
   server: sysname
   database_name: sysname
   database_user_name: sysname
   retry_attempts: int
   retry_interval: int
   os_run_priority: int
   output_file_name: nvarchar(200)
   last_run_outcome: int
   last_run_duration: int
   last_run_retries: int
   last_run_date: int
   last_run_time: int
   step_uid: uniqueidentifier
}
class sqlagent_jobsteps_logs {
   log_id: int
   log_text: nvarchar(max)
   date_created: datetime
   step_uid: uniqueidentifier
}
class sysallocunits {
   auid: bigint
   type: tinyint
   ownerid: bigint
   status: int
   fgid: smallint
   pgfirst: binary(6)
   pgroot: binary(6)
   pgfirstiam: binary(6)
   pcused: bigint
   pcdata: bigint
   pcreserved: bigint
}
class sysasymkeys {
   id: int
   name: sysname
   thumbprint: varbinary(64)
   bitlength: int
   algorithm: char(2)
   modified: datetime
   pkey: varbinary(4700)
   encrtype: char(2)
   pukey: varbinary(max)
}
class sysaudacts {
   class: tinyint
   id: int
   subid: int
   grantee: int
   audit_spec_id: int
   type: char(4)
   state: char(1)
}
class sysbinobjs {
   class: tinyint
   id: int
   nsid: int
   name: sysname
   status: int
   type: char(2)
   intprop: int
   created: datetime
   modified: datetime
}
class sysbinsubobjs {
   class: tinyint
   idmajor: int
   subid: int
   name: sysname
   status: int
   intprop: int
}
class sysbrickfiles {
   brickid: int
   dbid: int
   pruid: int
   fileid: int
   grpid: int
   status: int
   filetype: tinyint
   filestate: tinyint
   size: int
   maxsize: int
   growth: int
   lname: sysname
   pname: nvarchar(260)
   createlsn: binary(10)
   droplsn: binary(10)
   fileguid: uniqueidentifier
   internalstatus: int
   readonlylsn: binary(10)
   readwritelsn: binary(10)
   readonlybaselsn: binary(10)
   firstupdatelsn: binary(10)
   lastupdatelsn: binary(10)
   backuplsn: binary(10)
   diffbaselsn: binary(10)
   diffbaseguid: uniqueidentifier
   diffbasetime: datetime
   diffbaseseclsn: binary(10)
   redostartlsn: binary(10)
   redotargetlsn: binary(10)
   forkguid: uniqueidentifier
   forklsn: binary(10)
   forkvc: bigint
   redostartforkguid: uniqueidentifier
}
class syscerts {
   id: int
   name: sysname
   issuer: varbinary(884)
   snum: varbinary(32)
   thumbprint: varbinary(64)
   pkey: varbinary(4700)
   encrtype: char(2)
   cert: varbinary(max)
   status: int
   lastpkeybackup: datetime
}
class syschildinsts {
   lsid: varbinary(85)
   iname: sysname
   ipipename: nvarchar(260)
   pid: int
   status: int
   crdate: datetime
   modate: datetime
   sysdbpath: nvarchar(260)
}
class sysclones {
   id: int
   subid: int
   partid: int
   version: int
   segid: int
   cloneid: int
   rowsetid: bigint
   dbfragid: int
   status: int
}
class sysclsobjs {
   class: tinyint
   id: int
   name: sysname
   status: int
   type: char(2)
   intprop: int
   created: datetime
   modified: datetime
}
class syscolpars {
   id: int
   number: smallint
   colid: int
   name: sysname
   xtype: tinyint
   utype: int
   length: smallint
   prec: tinyint
   scale: tinyint
   collationid: int
   status: int
   maxinrow: smallint
   xmlns: int
   dflt: int
   chk: int
   idtval: varbinary(64)
}
class syscommittab {
   commit_ts: bigint
   xdes_id: bigint
   commit_lbn: bigint
   commit_csn: bigint
   commit_time: datetime
   dbfragid: int
}
class syscompfragments {
   cprelid: int
   fragid: int
   fragobjid: int
   ts: binary(8)
   status: int
   datasize: bigint
   itemcnt: bigint
   rowcnt: bigint
}
class sysconvgroup {
   id: uniqueidentifier
   service_id: int
   status: int
   refcount: int
}
class syscscolsegments {
   hobt_id: bigint
   column_id: int
   segment_id: int
   version: int
   encoding_type: int
   row_count: int
   status: int
   base_id: bigint
   magnitude: float
   primary_dictionary_id: int
   secondary_dictionary_id: int
   min_data_id: bigint
   max_data_id: bigint
   null_value: bigint
   on_disk_size: bigint
   data_ptr: binary(16)
   container_id: smallint
   bloom_filter_md: bigint
   bloom_filter_data_ptr: varbinary(16)
   collation_id: int
   min_deep_data: varbinary(18)
   max_deep_data: varbinary(18)
}
class syscsdictionaries {
   hobt_id: bigint
   column_id: int
   dictionary_id: int
   version: int
   type: int
   flags: bigint
   last_id: int
   entry_count: bigint
   on_disk_size: bigint
   data_ptr: binary(16)
   container_id: smallint
}
class syscsrowgroups {
   hobt_id: bigint
   segment_id: int
   version: int
   ds_hobtid: bigint
   row_count: int
   status: int
   flags: int
   compressed_reason: int
   generation: bigint
   created_time: datetime
   closed_time: datetime
   container_id: smallint
   blob_id: binary(16)
   metadata_offset: int
   metadata_size: int
}
class sysdbfiles {
   dbfragid: int
   fileid: int
   fileguid: uniqueidentifier
   pname: nvarchar(260)
}
class sysdbfrag {
   dbid: int
   fragid: int
   name: sysname
   brickid: int
   pruid: int
   status: int
}
class sysdbreg {
   id: int
   name: sysname
   sid: varbinary(85)
   status: int
   status2: int
   category: int
   crdate: datetime
   modified: datetime
   svcbrkrguid: uniqueidentifier
   scope: int
   cmptlevel: tinyint
}
class sysdercv {
   diagid: uniqueidentifier
   initiator: tinyint
   handle: uniqueidentifier
   rcvseq: bigint
   rcvfrag: int
   status: int
   state: char(2)
   lifetime: datetime
   contract: int
   svcid: int
   convgroup: uniqueidentifier
   sysseq: bigint
   enddlgseq: bigint
   firstoorder: bigint
   lastoorder: bigint
   lastoorderfr: int
   dlgtimer: datetime
   dlgopened: datetime
   princid: int
   outseskey: varbinary(4096)
   outseskeyid: uniqueidentifier
   farprincid: int
   inseskey: varbinary(4096)
   inseskeyid: uniqueidentifier
   farsvc: nvarchar(256)
   farbrkrinst: nvarchar(128)
   priority: tinyint
}
class sysdesend {
   handle: uniqueidentifier
   diagid: uniqueidentifier
   initiator: tinyint
   sendseq: bigint
   sendxact: binary(6)
}
class sysendpts {
   id: int
   name: sysname
   protocol: tinyint
   type: tinyint
   bstat: smallint
   affinity: bigint
   pstat: smallint
   tstat: smallint
   typeint: int
   port1: int
   port2: int
   site: nvarchar(128)
   dfltns: nvarchar(384)
   wsdlproc: nvarchar(776)
   dfltdb: sysname
   authrealm: nvarchar(128)
   dfltdm: nvarchar(128)
   maxconn: int
   encalg: tinyint
   authtype: tinyint
   encryptiontype: tinyint
}
class sysextendedrecoveryforks {
   dbid: int
   forkid: uniqueidentifier
   forklsn: binary(10)
}
class sysextfileformats {
   file_format_id: int
   name: nvarchar(128)
   format_type: nvarchar(100)
   field_terminator: nvarchar(10)
   string_delimiter: nvarchar(10)
   date_format: nvarchar(50)
   use_type_default: int
   serde_method: nvarchar(255)
   row_terminator: nvarchar(10)
   encoding: nvarchar(10)
   data_compression: nvarchar(255)
   first_row: int
   extractor: nvarchar(255)
   null_values: nvarchar(421)
   parser_version: nvarchar(8)
}
class sysextsources {
   data_source_id: int
   name: nvarchar(128)
   type_desc: nvarchar(255)
   type: tinyint
   location: nvarchar(4000)
   credential_id: int
   job_tracker_location: nvarchar(4000)
   storage_key: nvarchar(4000)
   user_name: nvarchar(128)
   shard_map_manager_db: nvarchar(128)
   shard_map_name: nvarchar(128)
   connection_options: nvarchar(4000)
   pushdown: nvarchar(256)
}
class sysexttables {
   object_id: int
   data_source_id: int
   file_format_id: int
   location: nvarchar(4000)
   reject_type: nvarchar(20)
   reject_value: float
   reject_sample_value: float
   sharding_dist_type: tinyint
   sharding_col_id: int
   source_schema_name: nvarchar(128)
   source_table_name: nvarchar(128)
   rejected_row_location: nvarchar(4000)
}
class sysfgfrag {
   fgid: int
   fgfragid: int
   dbfragid: int
   phfgid: int
   status: int
}
class sysfiles1 {
   status: int
   fileid: smallint
   name: nchar(128)
   filename: nchar(260)
}
class sysfoqueues {
   id: int
   lsn: binary(10)
   epoch: int
   csn: bigint
   created: datetime
}
class sysfos {
   id: int
   tgid: int
   low: varbinary(512)
   high: varbinary(512)
   rowcnt: bigint
   size: bigint
   csn: bigint
   epoch: int
   status: char(1)
   history: varbinary(6000)
   created: datetime
   modified: datetime
}
class sysftinds {
   id: int
   indid: int
   status: int
   crtype: char(1)
   crstart: datetime
   crend: datetime
   crrows: bigint
   crerrors: int
   crschver: binary(8)
   crtsnext: binary(8)
   sensitivity: tinyint
   bXVTDocidUseBaseT: tinyint
   batchsize: int
   nextdocid: bigint
   fgid: int
}
class sysftproperties {
   property_list_id: int
   property_id: int
   property_name: nvarchar(256)
   guid_identifier: uniqueidentifier
   int_identifier: int
   string_description: nvarchar(512)
}
class sysftsemanticsdb {
   database_id: int
   register_date: datetime
   registered_by: int
   version: nvarchar(128)
   fileguid: uniqueidentifier
}
class sysftstops {
   stoplistid: int
   stopword: nvarchar(64)
   lcid: int
   status: tinyint
}
class sysguidrefs {
   class: tinyint
   id: int
   subid: int
   guid: uniqueidentifier
   status: int
}
class sysidxstats {
   id: int
   indid: int
   name: sysname
   status: int
   intprop: int
   fillfact: tinyint
   type: tinyint
   tinyprop: tinyint
   dataspace: int
   lobds: int
   rowset: bigint
}
class sysiscols {
   idmajor: int
   idminor: int
   subid: int
   status: int
   intprop: int
   tinyprop1: tinyint
   tinyprop2: tinyint
   tinyprop3: tinyint
   tinyprop4: tinyint
}
class syslnklgns {
   srvid: int
   lgnid: int
   name: sysname
   status: int
   modate: datetime
   pwdhash: varbinary(320)
}
class syslogshippers {
   name: sysname
   psrv: sysname
   ssrv: sysname
   status: tinyint
   rolesequence: int
   safety: tinyint
   safetysequence: int
   logshippingid: uniqueidentifier
   familyid: uniqueidentifier
   statussequence: int
   witnesssequence: int
}
class sysmatrixageforget {
   usage: tinyint
   brick_id: int
   database_id: int
   recovery_unit_id: int
   persisted_age: bigint
   min_xact_begin_age: bigint
   offline_age: bigint
}
class sysmatrixages {
   age: bigint
   age_row_number: int
   age_issue_time: datetime
   age_content_version: tinyint
   age_contents: varbinary(8000)
}
class sysmatrixbricks {
   brick_id: int
   brick_guid: uniqueidentifier
   brick_state: int
   brick_config_state: int
   generation_id: bigint
   creation_time: datetime
   removal_time: datetime
   startup_time: datetime
   shutdown_time: datetime
   port_no: int
   host_name: nvarchar(64)
   instance_name: nvarchar(256)
   service_name: nvarchar(256)
   closed_age: bigint
   persisted_age: bigint
   offline_age: bigint
   incarnation_id: bigint
}
class sysmatrixconfig {
   param_id: int
   param_type: int
   param_int_value: bigint
   param_str_value: nvarchar(256)
}
class sysmatrixmanagers {
   brick_id: int
   manager_id: int
   manager_role: int
   minor_version: bigint
   major_version: bigint
   generation_id: bigint
}
class sysmultiobjrefs {
   class: tinyint
   depid: int
   depsubid: int
   indepid: int
   indepsubid: int
   status: int
}
class sysmultiobjvalues {
   valclass: tinyint
   depid: int
   depsubid: int
   indepid: int
   indepsubid: int
   valnum: int
   value: sql_variant
   imageval: varbinary(max)
}
class sysnsobjs {
   class: tinyint
   id: int
   name: sysname
   nsid: int
   status: int
   intprop: int
   created: datetime
   modified: datetime
}
class sysobjkeycrypts {
   class: tinyint
   id: int
   thumbprint: varbinary(32)
   type: char(4)
   crypto: varbinary(max)
   status: int
}
class sysobjvalues {
   valclass: tinyint
   objid: int
   subobjid: int
   valnum: int
   value: sql_variant
   imageval: varbinary(max)
}
class sysowners {
   id: int
   name: sysname
   type: char(1)
   sid: varbinary(85)
   password: varbinary(256)
   dfltsch: sysname
   status: int
   created: datetime
   modified: datetime
   deflanguage: sysname
   tenantid: uniqueidentifier
   onpremsid: varbinary(85)
   externaloid: uniqueidentifier
}
class sysphfg {
   dbfragid: int
   phfgid: int
   fgid: int
   type: char(2)
   fgguid: uniqueidentifier
   lgfgid: int
   status: int
   name: sysname
}
class syspriorities {
   priority_id: int
   name: sysname
   service_contract_id: int
   local_service_id: int
   remote_service_name: nvarchar(256)
   priority: tinyint
}
class sysprivs {
   class: tinyint
   id: int
   subid: int
   grantee: int
   grantor: int
   type: char(4)
   state: char(1)
}
class syspru {
   brickid: int
   dbid: int
   pruid: int
   fragid: int
   status: int
}
class sysprufiles {
   dbfragid: int
   fileid: int
   grpid: int
   status: int
   filetype: tinyint
   filestate: tinyint
   size: int
   maxsize: int
   growth: int
   lname: sysname
   pname: nvarchar(260)
   createlsn: binary(10)
   droplsn: binary(10)
   fileguid: uniqueidentifier
   internalstatus: int
   readonlylsn: binary(10)
   readwritelsn: binary(10)
   readonlybaselsn: binary(10)
   firstupdatelsn: binary(10)
   lastupdatelsn: binary(10)
   backuplsn: binary(10)
   diffbaselsn: binary(10)
   diffbaseguid: uniqueidentifier
   diffbasetime: datetime
   diffbaseseclsn: binary(10)
   redostartlsn: binary(10)
   redotargetlsn: binary(10)
   forkguid: uniqueidentifier
   forklsn: binary(10)
   forkvc: bigint
   redostartforkguid: uniqueidentifier
}
class sysqnames {
   qid: int
   hash: int
   nid: int
   name: nvarchar(4000)
}
class sysremsvcbinds {
   id: int
   name: sysname
   scid: int
   remsvc: nvarchar(256)
   status: int
}
class sysrmtlgns {
   srvid: int
   name: sysname
   lgnid: int
   status: int
   modate: datetime
}
class sysrowsetrefs {
   class: tinyint
   objid: int
   indexid: int
   rowsetnum: int
   rowsetid: bigint
   status: int
}
class sysrowsets {
   rowsetid: bigint
   ownertype: tinyint
   idmajor: int
   idminor: int
   numpart: int
   status: int
   fgidfs: smallint
   rcrows: bigint
   cmprlevel: tinyint
   fillfact: tinyint
   maxnullbit: smallint
   maxleaf: int
   maxint: smallint
   minleaf: smallint
   minint: smallint
   rsguid: varbinary(16)
   lockres: varbinary(8)
   scope_id: int
}
class sysrscols {
   rsid: bigint
   rscolid: int
   hbcolid: int
   rcmodified: bigint
   ti: int
   cid: int
   ordkey: smallint
   maxinrowlen: smallint
   status: int
   offset: int
   nullbit: int
   bitpos: smallint
   colguid: varbinary(16)
   ordlock: int
}
class sysrts {
   id: int
   name: sysname
   remsvc: nvarchar(256)
   brkrinst: nvarchar(128)
   addr: nvarchar(256)
   miraddr: nvarchar(256)
   lifetime: datetime
}
class sysscalartypes {
   id: int
   schid: int
   name: sysname
   xtype: tinyint
   length: smallint
   prec: tinyint
   scale: tinyint
   collationid: int
   status: int
   created: datetime
   modified: datetime
   dflt: int
   chk: int
}
class sysschobjs {
   id: int
   name: sysname
   nsid: int
   nsclass: tinyint
   status: int
   type: char(2)
   pid: int
   pclass: tinyint
   intprop: int
   created: datetime
   modified: datetime
   status2: int
}
class sysseobjvalues {
   valclass: tinyint
   id: bigint
   subid: bigint
   valnum: int
   value: sql_variant
   imageval: varbinary(max)
}
class syssingleobjrefs {
   class: tinyint
   depid: int
   depsubid: int
   indepid: int
   indepsubid: int
   status: int
}
class syssoftobjrefs {
   depclass: tinyint
   depid: int
   indepclass: tinyint
   indepname: sysname
   indepschema: sysname
   indepdb: sysname
   indepserver: sysname
   number: int
   status: int
}
class syssqlguides {
   id: int
   name: sysname
   scopetype: tinyint
   scopeid: int
   hash: varbinary(20)
   status: int
   created: datetime
   modified: datetime
   batchtext: nvarchar(max)
   paramorhinttext: nvarchar(max)
}
class systypedsubobjs {
   class: tinyint
   idmajor: int
   subid: int
   name: sysname
   xtype: tinyint
   utype: int
   length: smallint
   prec: tinyint
   scale: tinyint
   collationid: int
   status: int
   intprop: int
}
class sysusermsgs {
   id: int
   msglangid: smallint
   severity: smallint
   status: smallint
   text: nvarchar(1024)
}
class syswebmethods {
   id: int
   nmspace: nvarchar(384)
   alias: nvarchar(64)
   objname: nvarchar(776)
   status: int
}
class sysxlgns {
   id: int
   name: sysname
   sid: varbinary(85)
   status: int
   type: char(1)
   crdate: datetime
   modate: datetime
   dbname: sysname
   lang: sysname
   pwdhash: varbinary(256)
   tenantid: uniqueidentifier
   onpremsid: varbinary(85)
   externaloid: uniqueidentifier
}
class sysxmitbody {
   msgref: bigint
   count: int
   msgbody: varbinary(max)
}
class sysxmitqueue {
   dlgid: uniqueidentifier
   finitiator: bit
   tosvc: nvarchar(256)
   tobrkrinst: nvarchar(128)
   fromsvc: nvarchar(256)
   frombrkrinst: nvarchar(128)
   svccontr: nvarchar(256)
   msgseqnum: bigint
   msgtype: nvarchar(256)
   unackmfn: int
   status: int
   enqtime: datetime
   rsndtime: datetime
   dlgerr: int
   msgid: uniqueidentifier
   hdrpartlen: smallint
   hdrseclen: smallint
   msgenc: tinyint
   msgbodylen: int
   msgbody: varbinary(max)
   msgref: bigint
}
class sysxmlcomponent {
   id: int
   xsdid: int
   uriord: int
   qual: tinyint
   nameid: int
   symspace: char(1)
   nmscope: int
   kind: char(1)
   deriv: char(1)
   status: int
   enum: char(1)
   defval: nvarchar(4000)
}
class sysxmlfacet {
   compid: int
   ord: int
   kind: char(2)
   status: smallint
   dflt: nvarchar(4000)
}
class sysxmlplacement {
   placingid: int
   ordinal: int
   placedid: int
   status: int
   minoccur: int
   maxoccur: int
   defval: nvarchar(4000)
}
class sysxprops {
   class: tinyint
   id: int
   subid: int
   name: sysname
   value: sql_variant
}
class sysxsrvs {
   id: int
   name: sysname
   product: sysname
   provider: sysname
   status: int
   modate: datetime
   catalog: sysname
   cid: int
   connecttimeout: int
   querytimeout: int
}
class tbl_server_resource_stats {
   start_time: datetime2
   end_time: datetime2
   resource_type: nvarchar(128)
   resource_name: nvarchar(128)
   sku: nvarchar(128)
   hardware_generation: nvarchar(128)
   virtual_core_count: int
   avg_cpu_percent: decimal(5,2)
   reserved_storage_mb: bigint
   storage_space_used_mb: decimal(18,2)
   backup_storage_consumption_mb: decimal(18,2)
   io_requests_ read: bigint
   io_requests_ write: bigint
   io_bytes_read: bigint
   io_bytes_written: bigint
}
class trace_xe_action_map {
   trace_column_id: smallint
   package_name: nvarchar(60)
   xe_action_name: nvarchar(60)
}
class trace_xe_event_map {
   trace_event_id: smallint
   package_name: nvarchar(60)
   xe_event_name: nvarchar(60)
}

@enduml
